# Lubrificantes Industria e Perfuração

---
@snap[north-west]
#### Atualmente usamos:
@snapend

@snap[west span-50]

Oleos:

- 32: IPTUR AW 32
- 68: IPTUR AW 68
- 46: IPTUR HST 46

@snapend

@snap[east span-50]

Graxas:

- NLGI 3: GADUS S2 V220 3
- NLGI 2: GADUS S3 V220C 2 e 
GADUS S2 V220AD 2

@snapend
